# BfViewer
I have a large store of betting odds data from football matches across a number of european countries. All this data comes from the free BetFair API.
The BfViewer can be used to search through this data and will display a time series of prices for a given market.
The site is now running on http://54.77.101.112:1337/

Choose an event type, then search for a game using the input box. The available markets and runners in these markets can then be selected to get the price series in graphical form.

# Notes
**I have built a REST Api around this data, but currently the routes have no protection around modifying/deleting data. You cannot change the data by simply using the BFViewer as it's intended, however please avoid accessing the create, update and delete routes through the api directly!**

**The D3 chart that shows the price series is far from perfect, for example I would like to be able to scroll horizontally to see the full series. I'm considering this to be a minor step to take from here.**

**It can be a little slow to load the prices and the UI doesn't indicate this. Be patient! I would change this in the future to either fetch the data more dynamically or display some sort of loading control.**

# Stack
## Data Storage
All my data is stored in MongoDB on a standard linux instance on Amazon Web Services.
## Web Server
I have used Sails MVC framework to serve the site and build the REST Api.
## Client
I use Ember.js to create an SPA for the client. I'm also using libraries such as Bootstrap and D3 for styling and components.

