/**
 * Created by chris.lewis on 06/02/2015.
 */
module.exports={
  autoPk: true,
  tableName:'EventTypes',
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true
    },
    EventTypeId:{
      type:'integer'
    },
    Name:{
      type:'string'
    },
    ValidFrom:{
      type:'datetime'
    },
    ValidTo:{
      type:'datetime'
    }

  }
};
