/**
 * Created by chris.lewis on 18/02/2015.
 */
module.exports = {
  autoPk: true,
  tableName:'MarketCatelogue',
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true
    },
    MarketId:{
      type:'string'
    },
    MarketName:{
      type:'string'
    },
    EventId:{
      type:'string'
    }
  }

};
