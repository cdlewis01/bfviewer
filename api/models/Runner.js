/**
 * Created by chris.lewis on 18/02/2015.
 */
module.exports={
  autoPk: true,
  tableName:'RunnerDescriptions',
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true
    },
    MarketId:{
      type:'string'
    },
    RunnerName:{
      type:'string'
    },
    SelectionId:{
      type:'integer'
    }
  }
};
