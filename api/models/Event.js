/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  autoPk: true,
  tableName:'Events',
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true
    },
    EventId:{
      type:'integer'
    },
    Name:{
      type:'string'
    },
    ValidFrom:{
      type:'datetime'
    },
    ValidTo:{
      type:'datetime'
    },
    CountryCode:{
      type:'string'
    },
    Timezone:{
      type:'string'
    },
    OpenDate:{
      type:'datetime'
    }

  }
};

