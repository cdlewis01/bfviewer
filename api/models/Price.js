/**
 * Created by chris.lewis on 18/02/2015.
 */
module.exports={
  autoPk: true,
  tableName:'ExchangePrices',
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true
    },
    MarketId:{
      type:'string'
    },
    SelectionId:{
      type:'integer'
    },
    AvailableToBack:{
      type:'array'
    },
    AvailableToLay:{
      type:'array'
    },
    ValidFrom:{
      type:'datetime'
    },
    ValidTo:{
      type:'datetime'
    }
  }
};
