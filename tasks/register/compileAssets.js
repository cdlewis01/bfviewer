module.exports = function (grunt) {
	grunt.registerTask('compileAssets', [
		'bower:install',
    'clean:dev',
    'emberTemplates',
		'less:dev',
		'copy:dev',
		'coffee:dev'
	]);
};
