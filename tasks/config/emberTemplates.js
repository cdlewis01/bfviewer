/**
 * Created by chris.lewis on 06/02/2015.
 */
var pipeline = require('../pipeline');

module.exports = function (grunt) {
  grunt.config.set('emberTemplates', {
    compile: {
      options: {
        amd: false,
        templateBasePath: pipeline.templateBasePath
      },
      files: {
        '.tmp/public/jst.js': pipeline.templateFilesToInject
      }
    }
  });

  grunt.loadNpmTasks('grunt-ember-templates');
};
