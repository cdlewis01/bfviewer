/**
 * Copy files and folders.
 *
 * ---------------------------------------------------------------
 *
 * # dev task config
 * Copies all directories and files, exept coffescript and less fiels, from the sails
 * assets folder into the .tmp/public directory.
 *
 * # build task config
 * Copies all directories nd files from the .tmp/public directory into a www directory.
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-copy
 */
module.exports = function(grunt) {

  grunt.config.set('copy', {
    dev: {
      files: [{
        expand: true,
        cwd: './assets',
        src:['images/**/*.!(coffee|less|hbs)',
          'js/**/*.!(coffee|less|hbs)',
          'vendor/**/*.!(coffee|less|hbs|eot|svg|ttf|woff)',
          'styles/**/*.!(coffee|less|hbs)',
          '*.!(coffee|less|hbs)'],
        dest: '.tmp/public'
      },{ // hack to make sure that the bootstrap fonts get into the correct place
        expand:true,
        cwd:'./assets',
        flatten:true,
        src:['/glyphicons-halflings-regular.eot',
          '**/glyphicons-halflings-regular.svg',
          '**/glyphicons-halflings-regular.ttf',
          '**/glyphicons-halflings-regular.woff'],
        dest:'.tmp/public/vendor/fonts'
      }]

    },
    build: {
      files: [{
        expand: true,
        cwd: '.tmp/public',
        src: ['**/*'],
        dest: 'www'
      }]
    }
  });

	grunt.loadNpmTasks('grunt-contrib-copy');
};
