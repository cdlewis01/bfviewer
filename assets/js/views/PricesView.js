App.PricesView = Ember.View.extend({
  plotDataChanged: function () {
    var data = this.get('controller.plotData');
    var vis = d3.select('#chart');
    var WIDTH = 1100;
    var HEIGHT = 500;
    var MARGINS = {
      top: 20,
      right: 20,
      bottom: 20,
      left: 50
    };
    var xRange = d3.time.scale().rangeRound([MARGINS.left, WIDTH - MARGINS.right]).domain([d3.min(data, function(d) {
      return d.time;
    }), d3.max(data, function(d) {
      return d.time;
    })]);
    var yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([d3.min(data, function(d) {
      return Math.min(d.bestBack, d.bestLay);
    }), d3.max(data, function(d) {
      return Math.max(d.bestBack, d.bestLay);
    })]);
    var xAxis = d3.svg.axis()
      .scale(xRange)
      .orient('bottom')
      .ticks(d3.time.hours,1)
      .tickSize(0)
      .tickFormat(d3.time.format('%b %d %Y %I:%M'))
      .tickSubdivide(true);
    var yAxis = d3.svg.axis()
        .scale(yRange)
        .tickSize(0.5)
        .orient('left')
        .tickPadding(8).tickSubdivide(true);
    vis.append('svg:g')
      .attr('class','x axis')
      .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
      .call(xAxis);
    vis.append('svg:g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
      .call(yAxis);
    var backLineFunc = d3.svg.line()
      .x(function(dataPoint){
        return xRange(dataPoint.time);
      })
      .y(function(dataPoint){
        return yRange(dataPoint.bestBack);
      })
      .interpolate('linear');
    var layLineFunc = d3.svg.line()
      .x(function(dataPoint){
        return xRange(dataPoint.time);
      })
      .y(function(dataPoint){
        return yRange(dataPoint.bestLay);
      })
      .interpolate('linear');
    vis.append('svg:path')
      .attr('d',backLineFunc(data))
      .attr('stroke','blue')
      .attr('stroke-width',2)
      .attr('fill','none');
    vis.append('svg:path')
      .attr('d',layLineFunc(data))
      .attr('stroke','red')
      .attr('stroke-width',2)
      .attr('fill','none');
    var legendData = {
      0:["Back","blue"],
      1:["Lay","red"]
    };

    var legend = vis.append('g')
      .attr('class','legend')
      .attr('x',WIDTH - 65)
      .attr('y',25)
      .attr('height',100)
      .attr('width',100);
    legend.selectAll('rect')
      .data([0,1])
      .enter()
      .append("rect")
      .attr("x", WIDTH - 65)
      .attr("y", function(d,i){return i*25;})
      .attr("width", 10)
      .attr("height", 10)
      .style("fill", function(d) { return legendData[d][1] });

    legend.selectAll('text')
      .data([0,1])
      .enter()
      .append("text")
      .attr("x",WIDTH-52)
      .attr("y",function(d,i){return i*20 + 12;})
      .text(function(d){
        return legendData[d][0];
      });
  }.observes('controller.plotData').on('didInsertElement')
});



