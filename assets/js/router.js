App.Router.map(function () {
  this.resource('application',function(){
      this.route('index');
    }
  );
  this.resource('events',{path:'/events/:eventTypeId'});
  this.resource('markets',{path:'/markets/:eventId'});
  this.resource('runners',{path:'/runners/:marketId'});
  this.resource('prices',{path:'/prices/:selectionId'});
});

App.IndexRoute = Ember.Route.extend({
  model:function() {
    return this.store.find('eventtype');
  }
});

App.EventsRoute = Ember.Route.extend({
  queryParams:{
    eventTeam:{
      refreshModel:true
    }
  },
  model:function(params){
    return this.store.find('event',{where:'{"Name":{"contains":"'+params.eventTeam+'"}}'});
  }
});

App.MarketsRoute = Ember.Route.extend({
  model:function(params){
    return this.store.find('market',{EventId:params.eventId});
  }
});

App.RunnersRoute = Ember.Route.extend({
  model:function(params){
    this.set('m',params.marketId);
    return this.store.find('runner',{MarketId:params.marketId});
  },
  setupController:function(controller,model){
    controller.set('model',model);
    controller.set('mId',this.get('m'));
  }
});

App.PricesRoute = Ember.Route.extend({
  model:function(params){
    return this.store.find('price',{SelectionId:params.selectionId,MarketId:params.m});
  }
});


