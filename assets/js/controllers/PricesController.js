/**
 * Created by chris.lewis on 18/02/2015.
 */
App.PricesController = Ember.ObjectController.extend({
  queryParams:['m'],
  m:null,
  plotData:function(){
    var returnData = [];
    var currentBack =null;
    var currentLay = null;
    this.get('model').forEach(function(item,index){
      var availableToBack = item.get('AvailableToBack');
      var availableToLay = item.get('AvailableToLay');
      if(availableToBack && availableToBack.length>0){
        currentBack = availableToBack[0].Price;
      }
      if(availableToLay && availableToLay.length>0){
        currentLay = availableToLay[0].Price;
      }
      returnData.push({time:new Date(item.get('ValidFrom.DateTime')),bestBack:currentBack,bestLay:currentLay})
    });
    returnData.sort(function(x,y){
      if(x.time < y.time)return -1;
      else if(x.time> y.time)return 1;
      else return 0;
    });
    return returnData;
  }.property('model')
});
