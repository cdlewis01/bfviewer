/**
 * Created by chris.lewis on 18/02/2015.
 */
App.Price = DS.Model.extend({
  MarketId:DS.attr('string'),
  SelectionId:DS.attr('number'),
  AvailableToBack:DS.attr(),
  AvailableToLay:DS.attr(),
  ValidFrom:DS.attr(),
  ValidTo:DS.attr()
});
