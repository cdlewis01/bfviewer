/**
 * Created by chris.lewis on 18/02/2015.
 */
App.Runner = DS.Model.extend({
  MarketId:DS.attr('string'),
  RunnerName:DS.attr('string'),
  SelectionId:DS.attr('number')
});
