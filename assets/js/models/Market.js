/**
 * Created by chris.lewis on 18/02/2015.
 */
App.Market = DS.Model.extend({
  MarketId:DS.attr('string'),
  MarketName:DS.attr('string'),
  EventId:DS.attr('string')
});
