/**
 * Created by chris.lewis on 18/02/2015.
 */
App.Event = DS.Model.extend({
  EventId:DS.attr('number'),
  EventTypeId:DS.attr('number'),
  Name:DS.attr('string'),
  CountryCode:DS.attr('string'),
  Timezone:DS.attr('string')
});
